import React, { useState } from "react";
import "./App.css";
import { gql } from "apollo-boost";
import { useQuery } from "@apollo/react-hooks";

const GET_DOGS = gql`
  {
    dogs {
      id
      breed
    }
  }
`;

function Dogs({ onDogSelected }) {
  const { loading, error, data } = useQuery(GET_DOGS);

  if (loading) return "Loading...";
  if (error) return `Error! ${error.message}`;

  return (
    <select name="dog" onChange={onDogSelected}>
      {data.dogs.map((dog) => (
        <option key={dog.id} value={dog.breed}>
          {dog.breed}
        </option>
      ))}
    </select>
  );
}

const GET_DOG_PHOTO = gql`
  query Dog($breed: String!) {
    dog(breed: $breed) {
      id
      displayImage
    }
  }
`;

function DogPhoto({ breed }) {
  const { loading, error, data, refetch } = useQuery(GET_DOG_PHOTO, {
    variables: { breed },
    skip: !breed,
  });

  if (loading) return null;
  if (error) return `Error! ${error}`;

  return (
      <div>
        <img src={data.dog.displayImage} style={{ height: 100, width: 100 }} />
        <button onClick={() => refetch()}>Refetch!</button>
      </div>
  );
}

function App() {
  const [breed, setBreed] = useState("african");
  function setBreedValue(event) {
    setBreed(event.target.value);
  }
  return (
    <div>
      <Dogs onDogSelected={setBreedValue}></Dogs>
      <DogPhoto breed={breed}></DogPhoto>
    </div>
  );
}

export default App;
