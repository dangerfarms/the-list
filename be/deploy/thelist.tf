resource "google_cloud_run_service" "default" {
  name     = "thelist-api"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "gcr.io/dangerfarms"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}