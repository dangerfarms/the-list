ARG TAG

FROM gcr.io/danger-farms/dangerfarms-perks-api:$TAG
RUN apt-get install -y wget
RUN wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O /cloud_sql_proxy \
 && chmod +x /cloud_sql_proxy

COPY ./deploy/perks-service-account.json /app/perks-service-account.json

CMD ["./run-api-data-deploy.sh"]
