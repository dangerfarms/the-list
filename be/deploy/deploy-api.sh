#!/usr/bin/env sh
# Usage
# ./deploy-api.sh <STAGE>

# https://www.peterbe.com/plog/set-ex
set -ex

STAGE=$1

if [ ! "$STAGE" ]; then
  echo "You must provide a stage name e.g. './deploy-api.sh dev' for the stage name 'dev'"
  exit 1
fi

cd be/deploy

RUNTIME_ENV_FILE="../src/.env.$STAGE"

if [ ! -f "$RUNTIME_ENV_FILE" ]; then
  echo "The be/src/.env.$STAGE file does not exist. Please create it (based on .env.template)."
  exit 1
fi

# Load build-time envvars
. .env

# authenticate with gcloud using service account
gcloud auth activate-service-account --project="$PROJECT" --key-file=perks-service-account.json
gcloud auth configure-docker --quiet

docker pull $IMAGE || true

# build and push the API image
docker-compose build api
docker push "$IMAGE"

# build the data migration image (this depends on the api image)
docker-compose build api-data-deploy

# migrate data
docker-compose run api-data-deploy

# deploy a new revision of the api service to Cloud Run
# Ideally this would be managed by terraform, but currently the Cloud Run API is very minimal.
gcloud run deploy "$CLOUD_RUN_SERVICE_NAME" \
  --region="$REGION" \
  --project="$PROJECT" \
  --platform="managed" \
  --image="$IMAGE" \
  --add-cloudsql-instances="$INSTANCE_CONNECTION_NAME" \
  --set-env-vars="$(sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/,/g' $RUNTIME_ENV_FILE | sed -e "s/\$STAGE/$STAGE/g")" \
  --allow-unauthenticated
