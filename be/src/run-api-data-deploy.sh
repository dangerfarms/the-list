#!/usr/bin/env sh
set -e

/cloud_sql_proxy -dir=/cloudsql -instances=$INSTANCE_CONNECTION_NAME -credential_file=perks-service-account.json &
# wait for proxy to establish connection
sleep 5
./manage.py migrate
./manage.py collectstatic --no-input
