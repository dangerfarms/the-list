"""perks URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from graphene_django.views import GraphQLView

admin.site.site_header = "The List Administration"

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", GraphQLView.as_view(graphiql=True)),
]

# Here we add a development method for serving static media (used for both the admin site and graphiQL)
# For production, as part of the build process, we should be running `./manage.py collectstatic` to store
# any new media (js/css/img) in our STATIC_ROOT directory.
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
