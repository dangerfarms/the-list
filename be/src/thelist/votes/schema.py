import graphene

from graphene_django.types import DjangoObjectType

from thelist.votes.models import Vote


class VotesType(DjangoObjectType):
    class Meta:
        model = Vote


class Query(object):
    votes = graphene.List(VotesType)

    def resolve_votes(self, info, **kwargs):
        return Vote.objects.all()
