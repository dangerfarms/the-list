from django.contrib import admin

from thelist.votes.models import Vote


class VoteAdmin(admin.ModelAdmin):
    pass

admin.site.register(Vote, VoteAdmin)