from django.db import models

from thelist.common.models import BaseModel
from thelist.entities.models import Entity


class Vote(BaseModel):
    DOWN_VOTE = 'DV'
    UP_VOTE = 'UV'
    VOTE_TYPE_CHOICES = [
        (DOWN_VOTE, 'Downvote'),
        (UP_VOTE, 'Upvote'),
    ]

    entity = models.ForeignKey(Entity, on_delete=models.CASCADE)
    reason = models.TextField()
    type = models.TextField(max_length=2, choices=VOTE_TYPE_CHOICES)

    def __str__(self):
        return self.reason
