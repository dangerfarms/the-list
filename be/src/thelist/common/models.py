from uuid import uuid4

from django.db.models import Model, UUIDField, DateTimeField

from thelist.common.timezone import timezone_now


class BaseModel(Model):
    """A base for all models in the app."""

    class Meta:
        abstract = True

    id = UUIDField(primary_key=True, default=uuid4, unique=True, editable=False)
    created_at = DateTimeField(null=True)
    modified_at = DateTimeField(null=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        now = timezone_now()

        if self.created_at is None:
            self.created_at = now

        self.modified_at = now

        super().save()
