from datetime import datetime

from django.utils import timezone


def timezone_now():
    return datetime.now(tz=timezone.utc)
