import graphene
from django.conf import settings
from graphene_django.debug import DjangoDebug

import thelist.entities.schema
import thelist.votes.schema
import thelist.comments.schema


class Query(
    thelist.entities.schema.Query,
    thelist.votes.schema.Query,
    thelist.comments.schema.Query,
    graphene.ObjectType
):
    # Allow for SQL debugging through GraphQL API
    if settings.DEBUG:
        debug = graphene.Field(DjangoDebug, name='_debug')


schema = graphene.Schema(query=Query)
