import graphene
from graphene_django.types import DjangoObjectType

from thelist.comments.models import Comment


class CommentType(DjangoObjectType):
    class Meta:
        model = Comment


class Query(object):
    comments = graphene.List(CommentType)

    def resolve_comments(self, info, **kwargs):
        return Comment.objects.select_related('entity').all()
