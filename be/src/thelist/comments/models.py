from django.db import models


from thelist.common.models import BaseModel
from thelist.entities.models import Entity


class Comment(BaseModel):
    entity = models.ForeignKey(Entity, on_delete=models.CASCADE)
    comment = models.TextField()
