from django.db import models

from thelist.common.models import BaseModel


class Entity(BaseModel):
    name = models.TextField()

    def __str__(self):
        return self.name
