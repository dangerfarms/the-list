import graphene
from django.db.models import Count, Q, Sum, F
from django_filters import OrderingFilter, FilterSet, CharFilter
from graphene_django.filter import DjangoFilterConnectionField

from graphene_django.types import DjangoObjectType

from thelist.entities.models import Entity
from thelist.votes.models import Vote


class EntityFilter(FilterSet):
    class Meta:
        model = Entity
        fields = {
            'name': ['iexact', 'icontains'],
        }

    order_by = OrderingFilter(
        fields=(
            ('down_votes', 'down_votes'),
            ('up_votes', 'up_votes'),
            ('votes', 'votes'),
        )
    )


class EntityType(DjangoObjectType):
    down_votes = graphene.Int()
    up_votes = graphene.Int()
    votes = graphene.Int()

    class Meta:
        model = Entity
        interfaces = (graphene.relay.Node,)
        filterset_class = EntityFilter


class Query(object):
    entities = DjangoFilterConnectionField(EntityType)

    def resolve_entities(self, info, **kwargs):
        return Entity.objects \
            .annotate(down_votes=Count('vote', filter=Q(vote__type=Vote.DOWN_VOTE))) \
            .annotate(up_votes=Count('vote', filter=Q(vote__type=Vote.UP_VOTE))) \
            .annotate(votes=F('up_votes') - F('down_votes'))
