from django.contrib import admin

# Register your models here.
from thelist.entities.models import Entity


class EntityAdmin(admin.ModelAdmin):
    pass

admin.site.register(Entity, EntityAdmin)